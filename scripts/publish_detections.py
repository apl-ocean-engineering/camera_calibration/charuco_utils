#! /usr/bin/env python3

import cv2
import cv_bridge
import charuco_utils
import numpy as np
import rospy
import time
import tf

import apl_msgs.msg
import sensor_msgs.msg


class CharucoPublisher:
    def __init__(self, namespace, charuco_frame, board_name):
        self.namespace = namespace
        self.charuco_frame = charuco_frame
        self.board_name = board_name
        self.bridge = cv_bridge.CvBridge()
        self.aruco_dict, self.charuco_board = charuco_utils.get_charuco_board(
            self.board_name
        )

        self.center_x, self.center_y = charuco_utils.get_board_center(
            self.charuco_board
        )

        detection_topic = f"{self.namespace}/charuco_detections"
        self.detection_pub = rospy.Publisher(
            detection_topic, apl_msgs.msg.CharucoDetection, queue_size=1
        )

        self.br = tf.TransformBroadcaster()

        self.camera_info = None
        info_topic = f"{self.namespace}/camera_info"
        self.info_sub = rospy.Subscriber(
            info_topic, sensor_msgs.msg.CameraInfo, self.info_cb, queue_size=1
        )
        image_topic = f"{self.namespace}/image_raw"
        self.image_sub = rospy.Subscriber(
            image_topic, sensor_msgs.msg.Image, self.image_cb, queue_size=1
        )

        # Setup Publications
        tags_topic = f"{self.namespace}/annotated_tags"
        self.annotated_tags_pub = rospy.Publisher(
            tags_topic, sensor_msgs.msg.Image, queue_size=10
        )
        corners_topic = f"{self.namespace}/annotated_corners"
        self.annotated_corners_pub = rospy.Publisher(
            corners_topic, sensor_msgs.msg.Image, queue_size=10
        )

    def info_cb(self, msg):
        self.camera_info = msg

    def publish_annotated_images(
        self, camera_data, aruco_corners, aruco_ids, charuco_corners, charuco_ids
    ):
        try:
            annotated_corners = cv2.cvtColor(camera_data.copy(), cv2.COLOR_GRAY2RGB)
            corner_color = (255, 0, 0)
            annotated_corners = cv2.aruco.drawDetectedCornersCharuco(
                annotated_corners, charuco_corners, charuco_ids, corner_color
            )
            annotated_corners_msg = self.bridge.cv2_to_imgmsg(
                annotated_corners, encoding="rgb8"
            )
            annotated_corners_msg.header = self.camera_info.header
            self.annotated_corners_pub.publish(annotated_corners_msg)

            annotated_tags = cv2.cvtColor(camera_data.copy(), cv2.COLOR_GRAY2RGB)
            annotated_tags = cv2.aruco.drawDetectedMarkers(
                annotated_tags,
                aruco_corners,
                aruco_ids,
            )
            annotated_tags_msg = self.bridge.cv2_to_imgmsg(
                annotated_tags, encoding="rgb8"
            )
            annotated_tags_msg.header = self.camera_info.header
            self.annotated_tags_pub.publish(annotated_tags_msg)

        except Exception as ex:
            print(f"{ex}")

    def publish_detections(self, header, aruco_ids, charuco_ids, rvec, tvec):
        detection_msg = apl_msgs.msg.CharucoDetection()
        detection_msg.header = header
        detection_msg.board_name = self.board_name
        detection_msg.aruco_ids = list(aruco_ids.flatten())
        detection_msg.charuco_ids = list(charuco_ids.flatten())
        if rvec is not None:
            detection_msg.rvec = list(rvec.flatten())
        if tvec is not None:
            detection_msg.tvec = list(tvec.flatten())
        self.detection_pub.publish(detection_msg)

    def publish_tf(self, header, rvec, tvec):
        if rvec is None:
            rospy.logwarn("No board detected; cannot publish transform")

        else:
            rot, _ = cv2.Rodrigues(rvec)
            board_center = tvec + rot @ np.reshape(
                [self.center_x, self.center_y, 0.0], (3, 1)
            )

            angle = cv2.norm(rvec)
            qq = tf.transformations.quaternion_about_axis(angle, rvec.transpose())
            self.br.sendTransform(
                board_center,
                qq,
                header.stamp,
                self.charuco_frame,
                header.frame_id,
            )
            rpy = tf.transformations.euler_from_quaternion(qq)
            rospy.logwarn(f"rpy = {list(map(np.degrees, rpy))}")

    def image_cb(self, msg):
        if self.camera_info is None:
            rospy.logwarn(
                f"Received {self.namespace} image, but do not have cached camera_info"
            )
            return
        camera_data = self.bridge.imgmsg_to_cv2(msg, "mono8")
        (
            aruco_corners,
            aruco_ids,
            charuco_corners,
            charuco_ids,
            rvec,
            tvec,
        ) = charuco_utils.detect_charuco_board(
            self.aruco_dict,
            self.charuco_board,
            camera_data,
            self.camera_info,
            True,
            True,
        )

        self.publish_annotated_images(
            camera_data, aruco_corners, aruco_ids, charuco_corners, charuco_ids
        )

        self.publish_detections(msg.header, aruco_ids, charuco_ids, rvec, tvec)

        self.publish_tf(msg.header, rvec, tvec)


if __name__ == "__main__":
    rospy.init_node("charuco_publisher")
    # I don't love specifying a namespace and then hard-coding the topics,
    # but I'm not sure how else to do it and have it work on both log files
    # and when launched from within raven's launch files.
    left_ns = "/raven/stereo/left"
    right_ns = "/raven/stereo/right"
    board_name = "plinth_20201023_24x18"
    left_charuco = CharucoPublisher(left_ns, "left_charuco", board_name)
    right_charuco = CharucoPublisher(right_ns, "right_charuco", board_name)
    rospy.spin()
