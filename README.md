# Charuco Utils

Utilities for creating charuco boards and detecting them in camera images.

We had some issues where false tag detections caused checkerboard corners
to be assumed in entirely wrong locations, messing with calibration results.
(Tag 17 is particularly egregious)

To address this, we add a filtering step where we require tags to obey
some nearest-neighbor constraints before they are used in corners
(i.e. if for a given board, tag X and tag Y meet at a corner where X is
 above and to the left of Y, then tag Y must be the closest tag to
 tag X's lower right corner.)
This is not a perfect filter, but in practice it works well for us.

This repo includes some OPAL-specific code for generating named boards since
our research group is using charuco boards and aruco tags for various tasks
and may have more than one board / multiple markers in the scene at once.

Uses include:
* boards: these are generally meant for calibration targets
* plinths: these are also charuco boards, but intended to mount an object on
           to enable precise localization of the object for ground truth
           comparisons of different reconstructions approaches.
* tags: individual aruco tags mounted on tank wall/floor, used for localizing
        our sensors (or small robots)

For example, we don't remove navigation fiducials from the tank walls when
we want to calibrate a stereo pair.

If you just want to detect the standard board's position given ROS input
`ros_image` (sensor_msgs/Image) and `camera_info` (sensor_msgs/CameraInfo):
~~~
bridge = cv_bridge.CvBridge()
camera_data = bridge.imgmsg_to_cv2(ros_image, "mono8")
aruco_dict, charuco_board = get_charuco_board("default_11x8_45mm")  # Or define your own dictionary and board
_, _, _, _, rvec, tvec = detect_charuco_board(aruco_dict, charuco_board, camera_data, camera_info)
~~~
